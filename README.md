# A erlang based HTTP/1.1 HTTP/2 client 
An OTP application for creating http request to servers/APIs.
## How to:

### for http11:
```
-spec req(Method::binary(),Url::binary(),Header::httpTuple(),Body::httpTuple())->tuple().  
http11c:req(Method,<<"https://",Url/bitstring>>,{headers,HeaderLst},{body,[Body]})->

%%where:
Method :: binary()
Url::binary(),
Body :: binary()
-type httpTuple():: {atom(),tupleList()} | {atom(),[binary()]}.
-type tupleList():: list({bitstring(),bitstring()}).

```
### for http2:
```
http2c:start_link(ServerRef,URL,Port)-> start_ret() =
    {ok, Pid :: pid()} | ignore | {error, Reason :: term()}

%%where:
ServerRef :: atom() | pid()
URL :: list()
Port :: integer()
```
### using http2 in a supervision tree, it will send the preface and settings frame, everytime the worker is started.
So before the first http2 request is made, the "organizational frames" are already exchanged and the request can be done Immediately.
just add your desired server urls in the supervision tree of your program!
example:
```
#{id => test,
            %%ServerRef,Domain,Port
           start => {http2c,start_link,['test',"localhost",8080]},
		   restart => permanent,
		   shutdown => brutal_kill
		   }
```
