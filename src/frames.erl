-module(frames).
-export([generate/4,ping/0]).
-export([setting/4]).
ping() -> 
	Obq=crypto:strong_rand_bytes(8),
	<<8:24/integer,6:8/integer,0:7/integer,0:1/integer,0:1/integer,0:31/integer,Obq/bitstring>>.

setting(_sock,[],[],0) -> 
	<<0:24/integer,4:8/integer,0:7/integer,0:1/integer,0:1/integer,0:31/integer>>;

setting(_Sock,[{recbuf,Buf}],[],0) ->
	B = (Buf-9),
	%% no dyn. table ! SETTINGS_HEADER_TABLE_SIZE (0x01): == 0! 
	<<18:24/integer,4:8/integer,0:7/integer,0:1/integer,0:1/integer,0:31/integer,2:16/integer,0:32/integer,3:16/integer,99:32/integer,5:16/integer,B:32/integer>>.

generate(Sock,Header,<<>>,Sid) ->
	{settings,_Sock,_H,_Push,_C,_WS,Max,_}=db:read(settings,Sock),
	H=headers(Header,<<>>,Max,[1,Sid],Sock),
	ssl:send(Sock,H);

generate(Sock,Header,Data,Sid) ->
	{settings,_Sock,_H,_Push,_C,_WS,Max,_}=db:read(settings,Sock),
	H=headers(Header,<<>>,Max,[0,Sid],Sock),
	B=data(Data,Max,[Sid],Sock),
	ssl:send(Sock,[H,B]).

headers(KeyValLst,_Bin,Max,[ES,Sid],Sock) ->
	Hpack=hpack:encode(KeyValLst),
	L = byte_size(Hpack),
	if 
		L =< Max -> 
			<<L:24/integer,1:8/integer,0:5/integer,1:1/integer,0:1/integer,ES:1/integer,0:1/integer,Sid:31/integer,Hpack/bitstring>>;
		true -> continuation(KeyValLst,<<>>,Max,[9,Sid],Sock)
	end.

data(Payload,Max,[Sid],Sock) when byte_size(Payload) >= Max ->
	gen_frames(Payload,<<>>,Max,[0,Sid],Sock);

data(Payload,_Max,[Sid],_Sock) ->
	L = byte_size(Payload),
	%%ES =1
	<<L:24/integer,0:8/integer,0:4/integer,0:1/integer,0:2/integer,1:1/integer,0:1/integer,Sid:31/integer,Payload/bitstring>>.

%%% for header continuation 
continuation([],Bin,_Max,[T,Sid],Sock) ->
	L = byte_size(Bin),
	http2c:sending(Sock,<<L:24/integer,T:8/integer,0:5/integer,1:1/integer,0:2/integer,0:1/integer,Sid:31/integer,Bin/bitstring>>);

continuation([{K,V}|Headern],Bin,Max,[T,Sid],Sock)->
	L = byte_size(Bin),
	if 
		L =< Max -> 
			continuation(Headern,<<Bin/bitstring,K/bitstring,32,"=",32,V/bitstring,"\r\n">>,Max,[T,Sid],Sock);
		true ->
			http2c:sending(Sock,<<L:24/integer,T:8/integer,0:5/integer,0:1/integer,0:2/integer,0:1/integer,Sid:31/integer,Bin/bitstring,"\r\n">>),
			continuation(Headern,<<>>,Max,[T,Sid],Sock)
	end.

gen_frames(<<>>,Bin,_Max,[T,Sid],Sock) ->
	L = byte_size(Bin),
	http2c:sending(Sock,<<L:24/integer,T:8/integer,0:4/integer,0:1/integer,0:2/integer,1:1/integer,0:1/integer,Sid:31/integer>>);

gen_frames(Binary,Bin,Max,[T,Sid],Sock)->
	L = byte_size(Bin),
	http2c:sending(Sock,<<L:24/integer,T:8/integer,0:5/integer,0:1/integer,0:2/integer,0:1/integer,Sid:31/integer,Bin/bitstring>>),
	gen_frames(Binary,<<>>,Max,[T,Sid],Sock);

gen_frames(<<H:8,R/bitstring>>,Bin,Max,[T,Sid],Sock) when byte_size(Bin) =< Max ->
	gen_frames(R,<<Bin/bitstring,H/integer>>,Max,[T,Sid],Sock).
