%%%-------------------------------------------------------------------
%% @doc herlc top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(herlc_sup).

-behaviour(supervisor).

-export([start_link/2]).

-export([init/1]).
-define(SERVER, ?MODULE).

start_link(_Start,Args) ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, Args).

init(_AddChildspec) ->
	%% sup_flags() = #{strategy => strategy(),         % optional
	%%                 intensity => non_neg_integer(), % optional
	%%                 period => pos_integer()}        % optional
	%% child_spec() = #{id => child_id(),       % mandatory
	%%                  start => mfargs(),      % mandatory
	%%                  restart => restart(),   % optional
	%%                  shutdown => shutdown(), % optional
	%%                  type => worker(),       % optional
	%%                  modules => modules()}   % optional
	
	SupFlags = #{strategy => one_for_all,
		     intensity => 1,
		     period => 5},

	%Ch_specs=into_childspecs(AddChildspec,0,[]),

	Child = [
		 #{id => dbets,
		   start => {db,start_link,[]},
		   restart => permanent,
		   shutdown => brutal_kill
		  },
		 #{id => http2c1,
		   start => {http2c,start_link,[prescreen_api,"api.prescreenapp.io",443]},
		   restart => permanent,
		   shutdown => brutal_kill}
		],
	%All_childs= Child++Ch_specs,
	{ok, {SupFlags, Child}}.

%% internal functions
into_childspecs([],_Count,Acc)->
	Acc;

into_childspecs([[Servername,URL,Port]|R],Count,Acc) ->
	Id = <<"herlc_app",Count/integer>>,
	into_childspecs(R,Count+1,Acc ++ [
					  #{id => binary_to_atom(Id),
					    start => {http2c,start_link,[Servername,URL,Port]},
					    restart => permanent,
					    shutdown => brutal_kill}]).

