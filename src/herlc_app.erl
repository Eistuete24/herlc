%%%-------------------------------------------------------------------
%% @doc herlc public API
%% @end
%%%-------------------------------------------------------------------

-module(herlc_app).

-behaviour(application).

-export([start/2, stop/1]).

start(StartType, StartArgs) ->
    herlc_sup:start_link(StartType,StartArgs).

stop(_State) ->
    ok.

%% internal functions
