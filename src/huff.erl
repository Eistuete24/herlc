-module(huff).
-export([decode/1,decode/2]).
decode(Data) when is_binary(Data) ->
	{_,DecodedData}=decode(Data,<<>>,0),
	DecodedData.

decode(Data,Pid) when is_pid(Pid) -> 
	{_,DecodedData}=decode(Data,<<>>,0),
	Pid ! DecodedData.

decode(<<>>,Bin,_Char) ->
	{<<>>,Bin};
%%last header no  EOS? 
decode(<<15:4/integer>>,Decoded,0) -> 
	{<<>>,Decoded};

decode(<<H:5/integer,R/bitstring>>,Decoded,Char) ->
	{R3,Symb} = case ets:match(huff,{Char+H,'$1',5}) of 
			    [] ->
				    %%not found
				    add(R,Char+H,5);
			    [Ch]-> 
				    {R,list_to_binary(Ch)} 
		    end,
	decode(R3,<<Decoded/bitstring,Symb/binary>>,0).

%% For the stupid me, which will forget what is going on here
%% so we have an octect and no symbol found, so we use recusrion and call decode/3 again, where the third arg 
%% is the collected char from add.
%% this char argument gets concan.. gets added in the decode function to find the symbol where we look 
%% again at 5 bits at the same time. there it will call it the add function again, or find the value directly 
%% the result is the whole huff decoded string!
%%EOS
add(<<>>,127,_) -> 
	{<<>>,<<>>};

add(<<H:1,R/bitstring>>,Char,8) ->
	decode(R,<<>>,(Char+H)*2);
%% multiply by base of 2 
add(<<H:1,R/bitstring>>,Char,N) ->
	Char1 = if H ==1 
		   -> ((Char+H)*2)-1;
		   true -> (Char+H)*2
		end,
	case ets:match(huff,{Char1,'$1',N+1}) of
		[] -> 
			add(R,Char1,N+1);
		[Symb] -> 
			{R,list_to_binary(Symb)}
	end.

