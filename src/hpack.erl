-module(hpack).
-export([decode/1,encode/1]).

encode(KeyVal)->	
	encode(KeyVal,<<>>).

encode([],Hpack) when is_binary(Hpack) -> 
	Hpack;
encode([],Hpack) -> 
	padding(Hpack);

encode([{Key,Val}|N],Hpack) when (byte_size(Key) =<127) and (byte_size(Val) =<127) -> 
	Val_L = byte_size(Val),
	case ets:match(static_table,{'$1',Key}) of 
		[] -> HeaderField= match(Key,Val),
		      encode(N,<<Hpack/bitstring,HeaderField/binary>>);
		%% indexed name but value!
		[[Index]] ->
			Index1=potential_higher(Index),
			encode(N,<<Hpack/bitstring,0:3,0:1,Index1/bitstring,0:1,Val_L:7,Val/bitstring>>)
	end;

encode([{Key,Val}|N],Hpack) ->
	Val_L = byte_size(Val),
	case ets:match(static_table,{'$1',Key}) of 
		[] -> HeaderField= match(Key,Val),
		      encode(N,<<Hpack/bitstring,HeaderField/binary>>);
		%% indexed name but value!
		[[Index]] ->
			Index1=potential_higher(Index),
			Value_size_rest=adding(Val_L-127,<<>>),
			encode(N,<<Hpack/bitstring,0:3,0:1,Index1/bitstring,0:1,127:7,Value_size_rest/binary,Val/bitstring>>)
	end.


match(Key,Val) when (byte_size(Key)=<127) -> %or (byte_size(Val)=<127) -> 
	K_l =byte_size(Key),
	Val_L = byte_size(Val),
	case ets:match(static_table,{'$1',{Key,'_'}}) of 
		[] -> %%not in static_table
			if Val_L < 127 -> 
				   <<0:3,1:1,0:4,0:1,K_l:7,Key/bitstring,0:1,Val_L:7,Val/bitstring>>;
			   true -> Size = adding(Val_L-127,<<>>),
				   <<0:3,1:1,0:4,0:1,K_l:7,Key/bitstring,0:1,127:7,Size/binary,Val/bitstring>>
			end;

		[_H|_R] -> %% more then one but could also be new header
			case ets:match(static_table,{'$1',{Key,Val}}) of
				[] -> %% literal w/o and we dont use a dyn.table
					<<0:3,1:1,0:4,0:1,K_l:7,Key/bitstring,0:1,Val_L:7,Val/bitstring>>;

				[[Index]] -> 
					<<1:1,Index:7>>
			end
	end.
padding(Hpack) -> 
	List=bitstring_to_list(Hpack),
	Bitstring=lists:last(List),
	L=byte_size(Bitstring),
	G=byte_size(Hpack),
	Binary=extra(Bitstring),
	<<Hpack1:(G-L)/binary,_/bitstring>> = Hpack,
	<<Hpack1/binary,Binary/binary>>.

extra(Bin) when is_binary(Bin) -> 
	Bin;
extra(Bin) -> 
	extra(<<0:1,Bin/bitstring>>).

potential_higher(Index) when Index < 15 -> 
	<<Index:4>>;

potential_higher(Index) when Index >= 15 -> 
	Bin2=adding(Index-15,<<>>),
	<<15:4,Bin2/binary>>.

adding(Index,Bin) when Index < 127 ->
	<<Bin/binary,0:1,Index:7>>;

adding(Index,Bin) -> 
	adding(Index-127,<<Bin/bitstring,Index:7>>).
%%decode/1
decode(Msg) ->
	erlang:display(Msg),
	decode(Msg,[]).

decode(<<>>,Lst)-> 
	Lst;

%% Indexed Header Field!
decode(<<1:1,Index:7,R/bitstring>>,Lst)  ->
	{R1,FullIndex}=if 
			       Index >= 127 -> size_or_index(R,Index);
			       true -> {R,Index}
		       end,
	[Tuple] = ets:match(static_table,{FullIndex,'$1'}),
	decode(R1,Lst++Tuple);

%%fuck paypal! 
%decode(<<1:1,_Index:7,_R/bitstring>>,Lst)  ->
%	decode(<<>>,Lst);


%Literal Header Field with Incremental Indexing -- Indexed Name
decode(<<0:1,1:1,Index:6,R/bitstring>>,Lst) ->
	{R2,FullIndex,Value1} = if 
				 Index >= 64 -> indexed_name(R,Index);
				 true -> {R1,Value}=get_value(R),
					 {R1,Index,Value}
			 end,
	Header =case ets:match(static_table,{FullIndex,'$1'}) of
			[[{Name,_}]] -> Name;
			[[Name]] -> Name
		end,
	decode(R2,Lst++[{Header,Value1}]);

%% Literal Header Field with Incremental Indexing -- New Name
decode(<<64:8,R/bitstring>>,Lst) ->
	{R1,Name,Value}=new_name(R),
	decode(R1,Lst ++[{Name,Value}]);

%% Literal Header Field without Indexing -- Indexed Name
decode(<<0:4,PartIndex:4,R/bitstring>>,Lst) ->
	{R2,Index,Value}=if 
		PartIndex >= 15 -> indexed_name(R,PartIndex); 
		true -> {R1,Val}=get_value(R),
			{R1,PartIndex,Val}
			 end,
	[[Header]]=ets:match(static_table,{Index,'$1'}),
	decode(R2,Lst++[{Header,Value}]);

%%Literal Header Field without Indexing -- New Name
decode(<<0:8,Huff:1,R/bitstring>>,Lst) ->
	{R1,Name,Value}=new_name(R),
	Name1 =case Huff of 
		       1 -> huff:decode(Name);
		       0 -> Name
	       end,
	decode(R1,Lst ++[{Name1,Value}]);

%%Literal Header Field Never Indexed -- Indexed Name 
decode(<<1:4,PartIndex:4,R/bitstring>>,Lst) ->
	{R2,Index,Value} = if 
			     PartIndex <  15 -> {R1,Val}=get_value(R),
						{R1,PartIndex,Val};
			     true -> indexed_name(R,PartIndex)
		     end,
	[[Header]]=ets:match(static_table,{Index,'$1'}),
	decode(R2,Lst++[{Header,Value}]);

%Literal Header Field Never Indexed -- New Name
decode(<<16:8,R/bitstring>>,Lst)->
	{R1,Name,Value}=new_name(R),
	decode(R1,Lst++[{Name,Value}]);

%%table size update
decode(<<1:3,Table_size:5,R/bitstring>>,Lst)-> 
	if 
		Table_size < 31 -> ok;
		true -> ok
	end,
	decode(R,Lst).

%%Size = Index
size_or_index(<<0:1,Size:7,R/bitstring>>,N)  ->
	{R,N+Size};
%% 1 = continuation flag! 
size_or_index(<<1:1,Size:7,R/bitstring>>,N) -> 
	size_or_index(R,Size+N).

%%all hpack frames with new name should use this fuction!
new_name(<<Huff:1,0:1,Size:7,R/bitstring>>)->
	<<Name:Size/binary,R1/bitstring>> = R,
	{R3,Value}=get_value(R1),
		case Huff of 
		1 -> {R3,Name,huff:decode(Name)};
		0 -> {R3,Name,Value}
	end;

new_name(<<Huff:1,1:1,Size:7,R/bitstring>>)->
	{R,FullSize}=size_or_index(R,Size),
	<<Name:FullSize/binary,R1/bitstring>> = R,
	{R3,Value}=get_value(R1),
	case Huff of 
		1 -> {R3,Name,huff:decode(Name)};
		0 -> {R3,Name,Value}
	end.
%% cont flag=1 
indexed_name(<<1:1,PartIndex:7,R/bitstring>>,Index) -> 
	{R1,FullIndex} = size_or_index(R,PartIndex+Index),
	{R2,Value}=get_value(R1),
	{R2,FullIndex,Value};

%%continuation flag=0
indexed_name(<<0:1,PartIndex:7,R/bitstring>>,Index) -> 
	FullIndex = PartIndex+Index,
	{R2,Value}=get_value(R),
	{R2,FullIndex,Value}.

get_value(<<Huff:1,Value_l:7,R/bitstring>>) -> 
	{R1,VSize}=if 
			   Value_l >=127 -> size_or_index(R,Value_l);
			   true -> {R,Value_l}
		   end,
	<<Value:VSize/binary,R2/bitstring>> = R1,
	case Huff of 
		1 -> {R2,huff:decode(Value)};
		0 -> {R2,Value}
	end.
