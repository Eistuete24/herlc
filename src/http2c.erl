-module(http2c).
-behaviour(gen_server).
-export([start_link/3,start_link/4]).
-export([sending/2,tls/2]).
-export([read_socket_buffer/2]).
-export([init/1,terminate/2,handle_call/3,handle_cast/2]).
%-export([req/2]).
-export([req/5]).

sending(Sock,Msg) ->
	ssl:send(Sock,Msg).

start_link(ServerRef,Url,Port) ->
	gen_server:start_link({local,ServerRef},http2c,[Url,ServerRef,Port],[]).

start_link(ServerRef,Url,Port,Args) -> 
	gen_server:start_link({local,ServerRef},http2c,[Url,ServerRef,Port,Args],[]).

init([Url,ServerRef,Port]) ->
	%% starting the db_service
	{ok,Sock}=tls([Url],Port),
	{ok,Buf} = ssl:getopts(Sock,[recbuf]),
	ssl:setopts(Sock,Buf++[{mode,binary},{nodelay,true}]),
	SettFrame =frames:setting(Sock,Buf,[],0),
	ok=ssl:send(Sock,<<"PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n">>),
	ssl:send(Sock,SettFrame),
	M=preface(Sock),
	BufferPid=spawn_link(?MODULE,read_socket_buffer,[Sock,ServerRef]),
	{ok,[{sock,Sock},{sid,1},{buffer,BufferPid}]}.

%sep([]) -> 
%	ok;

%sep([H|R]) -> 
%	ok=file:write_file("/home/kangguru/erlangkey.log",H++["\n"],[append]),
%	sep(R).
%%preface
preface(Sock) -> 
	case ssl:recv(Sock,9) of 
		{ok,<<L:24/integer,T:8/integer,F:8/integer,0:1/integer,0:31/integer>>} ->
			{ok,Payload} =get_payload(Sock,L),
			http2_resp:settings(Payload,<<T,F>>,Sock);
		Reason ->
			error(Reason)
	end.

handle_call([{header,Headerlst},{body,Body}],{FPid,_Alias},[{sock,Sock},{sid,Sid},{buffer,BufferPid}]) ->
	frames:generate(Sock,Headerlst,Body,Sid),
	BufferPid ! {Sid,FPid},
	{reply,Sock,[{sock,Sock},{sid,Sid+2},{buffer,BufferPid}]}.

handle_cast(_Req,_State) -> 
	{reply,reply,state2}.

terminate(Reason,_State) -> 
	Reason.

%req(Link,{header,HeaderLst}) ->
%	Map=uri_string:parse(Link),
%	#{host := Authority} = Map, 
%	#{scheme := Scheme} = Map, 
%	#{path := Path} = Map,
%	gen_server:call('paypal.sand',[{header,[{<<":method">>, <<"POST">>},{<<":authority">>,Authority},{<<":scheme">>,Scheme},{<<":path">>,Path}] ++HeaderLst},{body,<<"{}">>}]),
%	receive 
%		Data -> Data
%	end.

-spec req(Method::binary(),ServerRef::pid(),Url::binary(),Header::http11c:httpTuple(),Body::http11c:httpTuple())->tuple().
req(Method,ServerRef,Link,{header,HeaderLst},{body,<<>>}) when is_atom(ServerRef) or is_pid(ServerRef), is_binary(Link) ->
	Map=uri_string:parse(Link),
	#{host := Authority} = Map, 
	#{scheme := Scheme} = Map, 
	#{path := Path} = Map,
	gen_server:call(ServerRef,[{header,[{<<":method">>, Method},{<<":authority">>,Authority},{<<":scheme">>,Scheme},{<<":path">>,Path}] ++HeaderLst},{body,<<>>}]),	
	receive 
		Data -> Data
	end.

%req(ServerRef,Link,{header,HeaderLst},{body,Body}) when is_atom(ServerRef) or is_pid(ServerRef), is_binary(Link) ->
%	Map=uri_string:parse(Link),
%	#{host := Authority} = Map, 
%	#{scheme := Scheme} = Map, 
%	#{path := Path} = Map,
%	gen_server:call(ServerRef,[{header,[{<<":method">>, <<"POST">>},{<<":authority">>,Authority},{<<":scheme">>,Scheme},{<<":path">>,Path}]++HeaderLst},{body,Body}]),	
%	receive 
%		Data -> Data
%	end.
tls([URL],Port) ->
	ok=ssl:start(permanent),
	{ok,Sock}  =ssl:connect(URL,Port,[{verify,verify_peer},
				    {cacerts, public_key:cacerts_get()},{active,false},
				    {packet,0},{mode,binary},
				    {alpn_advertised_protocols,[<<"h2">>]},
				    {versions,['tlsv1.2','tlsv1.3']}
				    ]),
	{ok,Sock}.

read_socket_buffer(Sock,ServerRef) -> 
	case ssl:recv(Sock,9) of 
		{ok,<<L:24,T:8,F:8,0:1,0:31>>} ->
			{ok,Payload}=get_payload(Sock,L),
			case T of 
				7 -> %%goaway
					spawn(http2_resp,get_msg,[Payload,T,Sock]),
					gen_server:stop(ServerRef);
				_ -> spawn(http2_resp,get_msg,[Payload,<<T,F>>,Sock]),
				     read_socket_buffer(Sock,ServerRef)
			end;
%%ES == 0
		{ok,<<L:24,T:8,F:7,0:1,0:1,Sid:31>>} -> 
			%% oh there must be a req made somewhere
			%% look at the pidMessagequeue
			%% should always be in order!
			receive 
				{Sid,ReqPid} ->
					{ok,Payload}=get_payload(Sock,L),
					Pid=spawn_link(http2_resp,get_msg,[Payload,<<T,F:7,0:1>>,ReqPid]),
					ok=read_further(Sock,Sid,Pid),
					read_socket_buffer(Sock,ServerRef)
			end;
		%% when connection failed!
		_ -> gen_server:stop(ServerRef) 
	end.

read_further(Sock,Sid,PrevPid) -> 
	case ssl:recv(Sock,9) of 
		%%ES == 0
		{ok,<<L:24,T:8,F:7,0:1,0:1,Sid:31>>} ->
			{ok,Payload}=get_payload(Sock,L),
					Pid=spawn_link(http2_resp,get_msg,[Payload,<<T,F:7,0:1>>,PrevPid]),
					read_further(Sock,Sid,Pid);
		%%ES == 1 
		{ok,<<L:24,T:8,F:7,1:1,0:1,Sid:31>>} ->
			{ok,Payload}=get_payload(Sock,L),
			spawn(http2_resp,get_msg,[Payload,<<T,F:7,1:1>>,PrevPid]),
			ok;
		_ -> read_further(Sock,Sid,PrevPid)
	end.

get_payload(_Sock,0) -> 
	{ok,<<>>};

get_payload(Sock,L) -> 
	ssl:recv(Sock,L).

