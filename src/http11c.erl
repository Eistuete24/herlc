-module(http11c).
-behaviour(gen_server).
-export([req/4]).
-export([header/2,body/2]).
-export([start_link/3]).
-export([init/1,handle_call/3,handle_cast/2,terminate/2]).
-type httpTuple():: {atom(),tupleList()} | {atom(),[binary()]}.
-type tupleList():: list({bitstring(),bitstring()}).
-export_type([httpTuple/0,tupleList/0]).
tls(URL,Port) ->
	ssl:connect(URL,Port,[{cacerts, public_key:cacerts_get()},{verify,verify_peer},	{customize_hostname_check, [{match_fun, public_key:pkix_verify_hostname_match_fun(https)}]},
			      {active,false},{packet,0},{mode,binary},{alpn_advertised_protocols,[<<"http/1.1">>]}]).
%start_link(["localhost"],Port) -> 
%	gen_server:start_link({local,pay},http11c,[["localhost"],Port],[]).

start_link(ServerRef,Url,Port) -> 
	gen_server:start_link({local,ServerRef},http11c,[Url,Port],[]).

init([Url,Port]) ->
	ok=ssl:start(permanent),
	{ok,[{Url,Port}]}.
	
terminate(Reason,State) ->
	{Reason,State}.

handle_cast(Req,State) ->
	{Req,State}.

handle_call([Method,Path,HeaderLst,Body],{_From,_Alias},[{Url,Port}]) ->
	{ok,Sock} = tls(Url,Port),
	Bin = generate(HeaderLst,<<Method/bitstring,32,Path/bitstring,32,"HTTP/1.1\r\n">>),
	ok=ssl:send(Sock,<<Bin/bitstring,Body/bitstring>>),
	Msg=get_resp(Sock),
	{reply,Msg,[{Url,Port}]}.

-spec req(Method::binary(),Url::binary(),Header::httpTuple(),Body::httpTuple())->tuple().
req(Method,<<"https://",Url/bitstring>>,{headers,HeaderLst},{body,[Body]})->
	Map=uri_string:normalize(<<"https://",Url/bitstring>>,[return_map]),
	#{host:= Host} = Map,
	#{path:= Path} = Map,
	#{query:=Query}=Map,
	L = byte_size(Body),
	HeaderLst1=HeaderLst++[{<<"Host">>,Host},{<<"User-Agent">>,<<"herlc/0.8">>},{<<"Content-Length">>,integer_to_binary(L)}],
	{ok,Sock} = tls(binary_to_list(Host),443),
	Bin = generate(HeaderLst1,<<Method/bitstring,32,Path/bitstring,"?",Query/bitstring,32,"HTTP/1.1\r\n">>),
	ok=ssl:send(Sock,<<Bin/bitstring,Body/bitstring>>),
	Msg=get_resp(Sock),
	Msg.

generate([],Bin) ->
	<<Bin/bitstring,"\r\n">>;


generate([{K,V}|HeaderN],Bin) ->
	generate(HeaderN,<<Bin/bitstring,K/bitstring,":",32,V/bitstring,"\r\n">>).

get_resp(Sock) -> 
	Data=get_resp(Sock,<<>>),
	Pid=spawn(?MODULE,body,[Data,self()]),
	spawn(?MODULE,header,[Data,Pid]),
	receive
		{StatCode,Header,Body} -> 
			{StatCode,Header,Body} 
	end,
	{StatCode,Header,Body}.

get_resp(Sock,Data) ->
	case ssl:recv(Sock,0) of
		{error,closed} -> Data;
		{ok,B}-> get_resp(Sock,<<Data/bitstring,B/bitstring>>)
	end.

statMsg(<<"\r\n",R/bitstring>>)->
	R;

statMsg(<<_:8/bitstring,R/bitstring>>)-> 
	statMsg(R).

header(<<"HTTP/1.1",32,StatCode:3/binary,32,R/bitstring>>,Pid) ->
	R2 = statMsg(R),
	header(R2,<<>>,{StatCode,[]},Pid).
%%/4
header(<<>>,_,{StatCode,Header},Pid)->
	Pid !{StatCode,Header};

header(<<"Transfer-Encoding: chunked",R/bitstring>>,_Bin,{StatCode,Header},Pid) -> 
	Pid ! chunked,
	header(R,<<>>,{StatCode,Header++[<<"Transfer-Encoding: chunked">>]},Pid);

header(<<13,10,13,10,_R/bitstring>>,Bin,{StatCode,Header},Pid) ->
	Pid ! {StatCode,Header++[Bin]};

header(<<13,10,R/bitstring>>,Bin,{S,H},Pid) -> 
	header(R,<<>>,{S,H++[Bin]},Pid);

header(<<H:8/bitstring,R/bitstring>>,Bin,Tpl,Pid) ->
	header(R,<<Bin/bitstring,H/bitstring>>,Tpl,Pid).

%% first time we get chunked Pid message | complete header, if chunked, we call the function again, and because
%% we have a message queue, with FIFO, the next message should be the {Stat,H} Pid message. Its always the last message from the header file! 
body(<<13,10,13,10,R/bitstring>>,MPid) -> 
	receive
		chunked -> 
			Body = body(R,<<>>,<<>>),
			body(<<13,10,13,10,Body/bitstring>>,MPid);

		{Stat,H} ->  
			MPid ! {Stat,H,R}
	end;

%%drop header
body(<<_:8/bitstring,R/bitstring>>,MPid)->
	body(R,MPid).

%%chunked body

%%last chunck ! finished
body(<<"0",13,10,13,10,R/bitstring>>,Bin,Body) ->
	file:write_file("/home/kangguru/aba.txt",Body),
	<<Body/bitstring,Bin/bitstring>>;

%%no chunked data
body(<<>>,Bin,Body) ->
	<<Body/bitstring,Bin/bitstring>>;

body(<<13,10,R/bitstring>>,Hex,Body) ->	
	Size=hex_to_integer(Hex),
	{R2,Chunk} = get_chunked_data(R,Size,<<>>),
	body(R2,<<>>,<<Body/bitstring,Chunk/bitstring>>);

body(<<H:8/bitstring,R/bitstring>>,Bin,Body) ->
	body(R,<<Bin/bitstring,H/bitstring>>,Body).

get_chunked_data(<<13,10,Binary/bitstring>>,Size,Chunk) when Size == byte_size(Chunk) ->
	{Binary,Chunk};

get_chunked_data(<<H:8/bitstring,R/bitstring>>,Size,Chunk) ->
	get_chunked_data(R,Size,<<Chunk/bitstring,H/bitstring>>).

hex_to_integer(Hex) ->
	if 
		byte_size(Hex) rem 2 == 0 -> binary:decode_unsigned(binary:decode_hex(Hex));
		true -> binary:decode_unsigned(binary:decode_hex(<<"0",Hex/bitstring>>))
	end.
