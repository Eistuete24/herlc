-module(huff_test).
-include_lib("eunit/include/eunit.hrl").
huff_test()->
	Msg = <<136,95,146,73,124,165,137,211,77,31,106,18,113,216,130,166,11,83,42,207,127,97,150,208,122,190,148,8,84,190,82,40,32,9,149,2,43,129,5,198,223,83,22,141,255>>,
	[?_assert(huff:decode(Msg) =:= {200,
 [{<<"content-type">>,<<"text/html; charset=utf-8">>},
  {<<"date">>,<<"Mon, 11 Dec 2023 12:26:57 GMT">>}],
 <<"<h1>Hello World</h1>">>})].
