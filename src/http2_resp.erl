-module(http2_resp).
-include("http2.hrl").
-export([get_msg/3]).
-export([settings/3]).
%%TODO woking on decoding header!
%% EH ES flag are set
get_msg(Header,<<1:8,0:4/integer,_PD:1/integer,1:1/integer,0:1/integer>>,Pid)-> 
	[{<<":status">>,StatCode}|Header1]=hpack:decode(Header),
	Pid ! {StatCode,Header1,<<>>};
%%
get_msg(Header,<<1:8,0:4/integer,_PD:1/integer,_EH:1/integer,0:1/integer,_ES:1/integer>>,Pid)-> 
	{StatCode,Header1}=case hpack:decode(Header) of 
				   [] -> {666,[]};
				   [{<<":status">>,Stat}|H] -> {Stat,H}
			   end,
	receive 
		{d,D} -> Pid ! {StatCode,Header1,D};
		{c,C} -> Pid ! {StatCode,<<Header1/bitstring,C/bitstring>>,<<>>};
		{C,D} -> Pid ! {StatCode,<<Header1/bitstring,C/bitstring>>,D};
		{err,Msg,Code} -> Pid ! {err,Header1,Msg,Code}
	end;
%%body
get_msg(Payload,<<0:8/integer,0:4/integer,_P:1/integer,0:2/integer,ES:1/integer>>,Pid)->
	case ES of 
		1 -> Pid ! {d,Payload};
		_ -> receive 
			     {d,D} -> Pid ! {d,<<Payload/bitstring,D/bitstring>>};
			     {err,Code} -> Pid ! {err,Payload,Code}
		     end
	end;
get_msg(ErrorCode,<<3:8/integer,0:8/integer,0:1/integer>>,Pid) -> 
	Pid ! {err,ErrorCode};

%%settings ACK 
get_msg(<<>>,<<4:8/integer,0:7/integer,1:1/integer>>,_Sock) ->
	ok;
%%settings
get_msg(Settings,<<4:8/integer,0:7/integer,0:1/integer>>,Sock) ->
	Rec=human_readable(Settings),
	R2 = Rec#settings{sock = Sock},
	true=db:write(R2),
	%% send ACK back
	ok=http2c:sending(Sock,<<0:24/integer,4/integer,0:7/integer,1:1/integer,0:1/integer,0:31/integer>>);
%%RST_Stream
get_msg(Protocol_error,<<3/integer,_F/integer>>,SidPid) -> 
	SidPid ! Protocol_error;

%% push promise
get_msg(_FBF,<<5:8/integer,0:4/integer,_P:1/integer,_EH:1/integer,0:2/integer>>,Sock) -> 
	Frame=http2_resp:error(1),
	ssl:send(Sock,Frame);
%%ping!
get_msg(Data,<<6:8/integer,0:7/integer,ACK:1/integer>>,Sock) -> 
	case ACK of 
		<<1:1>> -> ok; 
		_ -> % send identical Data back with ACK = 1 
			L  = byte_size(Data),
			ssl:send(Sock,<<L:24/integer,6:8/integer,0:7/integer,1:1/integer,0:1/integer,0:31/integer,Data/bitstring>>)
	end;
%%goaway
get_msg(<<0:1/integer,_LastID:31,_ErrorCode:32,_AddDebug/bitstring>>,7,Sock) -> 
	%%send goaway; and terminate
	%<<_:30,Nbr:1/integer>> = LastID,
	%% cheap workaround
	%LastprocID=Nbr,	
	ssl:send(Sock,<<64:32,7:8/integer,0:9/integer,0:31,0:1,0:31/integer,0:32/integer>>);
%%window update
get_msg(WindowSize,<<8:8,_F:8>>,Sock) -> 
	Rcd=db:read(settings,Sock),
	R1 = Rcd#settings{initial_window_size=binary:decode_unsigned(WindowSize,big)},
	db:write(R1),
	ok;
%%continuation
get_msg(FBF,<<9:8/integer,0:5/integer,EH:1/integer,0:2/integer>>,Pid) -> 
	case EH of 
		1  -> 
			Pid ! {c,FBF};
		_ -> receive 
			     {c,C} -> Pid ! {c,<<FBF/bitstring,C/bitstring>>};
			     {err,Code} -> Pid ! {err,FBF,Code} 
		     end
	end.
%%default vals
human_readable(<<>>) ->
	#settings{header_table_size=4096, enable_push=1,max_concurrent_streams=99,initial_window_size=65535,max_frame_size=16384,max_header_list_size=10000};

human_readable(<<Id:16/integer,Value:32/integer,R/bitstring>>) ->
	P1 = case Id of 
		     1 -> #settings{header_table_size=Value};
		     2 -> #settings{enable_push=Value};
		     3 -> #settings{max_concurrent_streams=Value};
		     4 -> #settings{initial_window_size=Value};
		     5 -> #settings{max_frame_size=Value};
		     6 -> #settings{max_header_list_size=Value}
	     end,
	human_readable(R,P1).

human_readable(<<>>,Rcd) -> 
	Rcd;

human_readable(<<Id:16/integer,Value:32/integer,R/bitstring>>,P) ->
	P1 = case Id of 
		     1 -> P#settings{header_table_size=Value};
		     2 -> P#settings{enable_push=Value};
		     3 -> P#settings{max_concurrent_streams=Value};
		     4 -> P#settings{initial_window_size=Value};
		     5 -> P#settings{max_frame_size=Value};
		     6 -> P#settings{max_header_list_size=Value}
	     end,
	human_readable(R,P1).
%%settings ACK 
settings(<<>>,<<4:8/integer,0:7/integer,1:1/integer>>,_Sock) ->
	ok;
%%settings
settings(Settings,<<4:8/integer,0:7/integer,0:1/integer>>,Sock) ->
	Rec=human_readable(Settings),
	R2 = Rec#settings{sock = Sock},
	Msg=db:write(R2),
	%% send ACK back
	ok=http2c:sending(Sock,<<0:24/integer,4/integer,0:7/integer,1:1/integer,0:1/integer,0:31/integer>>).

